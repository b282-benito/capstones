import {useContext, useEffect} from 'react';

import UserContext from '../UserContext';

import {Navigate} from 'react-router-dom';

export default function Logout() {

    const {unsetUser, setUser} = useContext(UserContext);

    // localStorage.clear();
    unsetUser();

    useEffect(() => {
        setUser({email: null})
    });

    return (
        <Navigate to="/login" />
    )
}
