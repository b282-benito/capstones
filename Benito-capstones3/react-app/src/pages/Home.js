import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

  const data = {
    title: "Tastea Alley PH",
    content: "refreshment in every cup!",
    destination: "/UserProducts",
    label: "Order Now!"
  }


  return (
    <>
    <Banner data={data} />
      <Highlights />
      
    </>
  )
}
