import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

export default function CreateProduct() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    // to store values of the input fields
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

useEffect(() => {
    if((name !== "" && description !== "" && price !== "")) {

        setIsActive(true)
    } else {
        setIsActive(false)
    }
}, [name, description, price])

    // function to simulate user registration
    function addProduct(e) {
        // Prevents page from reloading       
        e.preventDefault();


                fetch(`${process.env.REACT_APP_API_URL}/product/create`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {

                        // Clear input fields
                        setName("");
                        setDescription("");
                        setPrice("");

                        Swal.fire({
                            title: "Succesfully Added New Product",
                            icon: "success",
                            text: "Product Added!"
                        })

                        navigate("/products");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }

                })
            // }
        // })
    }


    return (
        <Form onSubmit={(e) => addProduct(e)} >

                    <h1 className="text-center my-3">Add New Product</h1>

                    <Form.Group className="mb-3" controlId="productName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                        type="text"
                        value={name}
                        onChange={(e) => {setName(e.target.value)}}
                        placeholder="Enter Product Name" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="productDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={description}
                        onChange={(e) => {setDescription(e.target.value)}}
                        placeholder="Enter Product Description" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="productPrice">
                    <Form.Label>Price</Form.Label>
                        <Form.Control 
                        type="text" 
                        value={price}
                        onChange={(e) => {setPrice(e.target.value)}}
                        placeholder="Enter Product Price" />
                    </Form.Group>
                  { isActive ?
                            <Button variant="primary" type="submit" id="submitBtn">
                             Submit
                            </Button>
                            :
                            <Button variant="primary" type="submit" id="submitBtn" disabled>
                              Submit
                            </Button>
                  }

                 
                </Form> 
    )
}