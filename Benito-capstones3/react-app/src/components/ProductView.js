import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();

  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [totalAmount, setTotalAmount] = useState(0);

  const QuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value, 10);
    setQuantity(newQuantity);
    setTotalAmount(price * newQuantity);
  };

  const checkout = () => {


  fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify({
    	productId: productId,
    	productName: productName,
    	quantity:quantity,
    	totalAmount: totalAmount,
    })
  })
    .then((res) => res.json())
    .then((data) => {
      console.log('Checkout response:', data);

      if (data) {
        Swal.fire({
          title: 'Ordered Successfully',
          icon: 'success',
          text: 'You have placed an order.',
        });
        navigate('/UserProducts');
      } else {
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.',
        });
      }
    })
    .catch((error) => {
      console.error('Error while checking out:', error);
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'Something went wrong. Please try again later.',
      });
    });
};

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        //setName(data.name);
        setProductName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setTotalAmount(data.price * quantity); // Calculate initial total amount
      })
      .catch((error) => {
        console.error('Error fetching product details:', error);
      });
  }, [productId, quantity]);

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{productName}</Card.Title>
              <br/>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              <input type="number" value={quantity} onChange={QuantityChange} />
              <div>
              <Card.Subtitle>Total Amount:</Card.Subtitle>
              <Card.Text>PhP {totalAmount}</Card.Text>
              </div>
              {user.id !== null ? (
                <Button variant="primary" onClick={() => checkout(productId)}>
                  Checkout
                </Button>
              ) : (
                <Button className="btn btn-danger" as={Link} to="/login">
                  Log in to Checkout
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
