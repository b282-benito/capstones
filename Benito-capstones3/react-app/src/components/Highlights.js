import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>MILKTEA</h2>
	                    </Card.Title>
	                    <Card.Text>
						Milk tea refers to several forms of beverage found in many cultures, consisting of some combination of tea and milk. The term milk tea is used for both hot and cold drinks that can be combined with various kinds of milks and a variety of spices.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>FRUIT TEA</h2>
	                    </Card.Title>
	                    <Card.Text>
	                     Fruit tea is an infusion made from cut pieces of fruit and plants, which can either be fresh or dried. By definition in the guidelines of German food law, fruit tea is a tea-like beverage as it is not produced using the same traditional method as for green tea or black tea.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>PREMIUM BLENDS</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}
