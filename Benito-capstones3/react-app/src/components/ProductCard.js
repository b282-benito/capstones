
import {useState, useEffect} from 'react';

import { Button, Row, Col, Card } from 'react-bootstrap';

import {Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function ProductCard({product}) {

const {name, description, price, isActive, _id} = product;
const [products, setProducts] = useState([]);
const [btnClass, setBtnClass] = useState();
const navigate = useNavigate();


    function archiveProduct(productId, e) {
        fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/archive`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then((res) => res.json())
        .then((data) => {
            console.log('Product archived:', data);
            setProducts((prevProducts) =>
            prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: false } : product
            )
        );
            // Swal.fire({
            //     title: "Product deactivation Successful",
            //     icon: "success",
            //     text: "Deactivated!!!"
            //  })
            window.location.reload();

        })
        .catch((error) => {
            console.error('Error archiving product:', error);
        });

    }





    function activatingProduct(productId) {
        fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/activate`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
            isActive: true
        })
        })
        .then((res) => res.json())
        .then((data) => {
            console.log('Product activated:', data);

            setProducts((prevProducts) =>
            prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: true } : product
            )
            );
            // Swal.fire({
            //     title: "Product Activation Successful",
            //     icon: "success",
            //     text: "Activated"
            //  })
            window.location.reload();

        })
        .catch((error) => {
            console.error('Error activating product:', error);
        });
 
    }






return (

    <Row className="mt-3 mb-3">
        <Col xs={12}>
            <Card className="cardHighlight p-0">
                <Card.Body class="text-center">
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Button className="bg-primary" as={Link} to={`/product/${_id}/update`} >Update</Button>
{/*            <Button
              variant={isActive ? 'success' : 'danger'}
              onClick={isActive ? archiveProduct(product._id) : activatingProduct(product.id)}
            >
              {isActive ? 'Active' : 'Archived'}
            </Button>*/}

               {
                (product.isActive)?
                <Button className="bg-success" onClick={() => archiveProduct(product._id, )} >Deactivate</Button>
                :
                <Button className="bg-secondary" onClick={() => activatingProduct(product._id)} >Activate</Button>
                }


                </Card.Body>
            </Card>
        </Col>
    </Row>       
    )
}



