const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};


// User registration

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if (error) {
			return false;
		// User registration successful
		} else {
			return true;
		};
	});
};


// User authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }
			} else {
				return false;
			};
		};
	});
};


// Controllers for non admin user checkout
module.exports.checkout = async (data, userId) => {
const user = await User.findById(userId).exec();
  if (user.isAdmin) {
    return false;
  }

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orderedProduct.push({
			  products: [{
			    productId: data.productId,
			    productName: data.productName,
			    quantity: data.quantity,
			  }],
			  totalAmount: data.totalAmount,
			});
		console.log(user);
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	});


	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.userOrders.push({userId: data.userId});
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	});


	if(isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	};
};








// Retrieve user details
module.exports.getProfile = (data) => {
return User.findById(data.userId).then(result => {
	
	result.password = "";

	return result;
	});
};

