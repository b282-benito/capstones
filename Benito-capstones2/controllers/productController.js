const Product = require("../models/Product");

// Create a product
module.exports.addProduct = (data) => {
	if (data.isAdmin) {

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			// Product creation successful
			if (error) {
				return false;
			// Product creation failed
			} else {
				return true;
			};
		});

	};

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


// Controllers for retrieving all the products

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Controllers for retrieving active products

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Controllers for retrieving specific products

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Controllers for updating a product

module.exports.updateProduct = (productId, data) => {

	if (data.isAdmin) {
		let updatedProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		};

		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};

// Controllers for archiving a product
module.exports.archiveProduct = (reqParams) => {
    let archiveProduct = {
        isActive: false
    };
    return Product.findByIdAndUpdate(reqParams.productId,archiveProduct).then((product, error) => {
        if(error) {
            return false;
        } else {
            return true;
        };
    });
};


module.exports.activatingProduct = (reqParams) => {
    let activateProduct = {
        isActive: true
    };
    return Product.findByIdAndUpdate(reqParams.productId,activateProduct).then((product, error) => {
        if(error) {
            return false;
        } else {
            return true;
        };
    });
};